FROM docker:stable

#Add 3.16 repos to install docker-compose 1.29 on alpine 3.12
RUN echo -e "#/media/cdrom/apks\nhttp://ftp.halifax.rwth-aachen.de/alpine/v3.16/main\nhttp://ftp.halifax.rwth-aachen.de/alpine/v3.16/community\n" >> /etc/apk/repositories

RUN  apk update --allow-untrusted && apk add --allow-untrusted curl make bash git openssh-client py-pip python3-dev libffi-dev rust cargo openssl-dev gcc libc-dev gettext docker-compose

#Add 3.18 repos to install helm on alpine 3.12
RUN echo -e "#/media/cdrom/apks\nhttp://ftp.halifax.rwth-aachen.de/alpine/v3.18/main\nhttp://ftp.halifax.rwth-aachen.de/alpine/v3.18/community\n" >> /etc/apk/repositories

RUN apk --allow-untrusted add helm
